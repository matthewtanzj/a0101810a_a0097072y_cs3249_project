import React from 'react';
import { Meteor } from 'meteor/meteor';
import { render } from 'react-dom';
import { renderRoutes } from '../imports/startup/client/reactRouter.jsx';

import '../imports/api/client/auth.js';
import '../imports/api/lib/events.js';

Meteor.startup(() => {
    render(renderRoutes(), document.getElementById('app'));
});