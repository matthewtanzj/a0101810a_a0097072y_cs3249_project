import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';
 
// stores the collection for events
export const Events = new Mongo.Collection('events');

if (Meteor.isServer) {
    // This code only runs on the server
    Meteor.publish('events', function eventsPublication() {
        return Events.find();
    });
    Meteor.publish('event', function(id) {
        return Events.find({
            _id: id
        });
    });
}

// stub methods for CRUD of event collection
Meteor.methods({
    'events.insert'(eventObj, callback) {
        var err = undefined;
        const checkTitle = check(eventObj.title, String);
        const checkOrganizer = check(eventObj.organizer, String);
        if(checkTitle || checkOrganizer) {
            err = [];
            err.push(checkTitle);
            err.push(checkOrganizer);
        } else {
            Events.insert({
                title: eventObj.title,
                organizer: eventObj.organizer,
                committee: eventObj.committee || "",
                category: eventObj.category || "",
                tags: eventObj.tags || "",
                description: eventObj.description || "",
                startDate: eventObj.startDate || null,
                endDate: eventObj.endDate || null,
                dateTime: eventObj.dateTime || "",
                venue: eventObj.venue || "",
                price: eventObj.price || 0,
                agenda: eventObj.agenda || "",
                contact: eventObj.contact || "",
                hits: 0,
                owner: Meteor.userId()
            });
        }
        if(callback)
            callback(err);
    },
    'events.remove'(id) {
        Events.remove(id, function(err) {
            if(err) {
                console.log('error removing event');
                console.log(err);
            } else {
                console.log("successfully removed event");
            }
        });
    },
    'events.removeall'() {
        Events.remove({});
    },
    /* For testing purposes */
    'events.addDummy'() {
        Meteor.apply('events.insert', [{title: "Paid Study ($5)", organizer: "ODAC Club", dateTime: "Ongoing?"}]);
        Meteor.apply('events.insert', [{title: "Project HAK 2016 Recruitment", organizer: "NUS Rotaract Club", dateTime: "December break"}]);
        Meteor.apply('events.insert', [{title: "NUS Student Leadership Camp", organizer: "OSA", dateTime: "6 - 8 May 2016."}]);
        Meteor.apply('events.insert', [{title: "Come volunteer with Kaleidoscope Project Ignite!", organizer: "NUS Student Union Volunteer Action Committee (NVAC)", dateTime: "4-6 June"}]);
        Meteor.apply('events.insert', [{title: "Oral Communication Camp", organizer: "CELC", dateTime: "18 - 20 May 2016"}]);
    },
    'events.showall'() {
        return Events.find({}).fetch();
    }
});