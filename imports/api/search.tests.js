/* eslint-env mocha */

import { assert } from 'meteor/practicalmeteor:chai';
import {findDOMNode, render} from 'react-dom';
import Event from '../ui/components/Event';
import EventList from '../ui/components/EventList';
import EventForm from '../ui/components/EventForm';
// import {EventViewWrapper} from '../ui/component/EventViewWrapper';

if (Meteor.isClient) {
    var React = require('react');
    var ReactDOM = require('react-dom');
	var TestUtils = require('react-addons-test-utils');
	
	describe('Event Component', () => {

		describe('can render a table row', () => {
			
			before(function() {
				const FAKE_DOM_HTML = '<html><body></body></html>';
				if (typeof document !== 'undefined') {
					return;
				}
				global.document = jsdom.jsdom(FAKE_DOM_HTML);
				global.window = document.defaultView;
				global.navigator = window.navigator;
			});
			
			beforeEach(function() {
				
			});
			
			it('that has the correct title', () => {
				var event = {title:"This is an event", organizer:"NUS", dateTime:"Ongoing"};
				let renderedEvent = TestUtils.renderIntoDocument(<Event event={event} />);
				const title = renderedEvent.refs.title;
				assert.equal(title.textContent, event.title);
			});
			
			it('that has the correct organizer', () => {
				var event = {title:"This is an event", organizer:"NUS", dateTime:"Ongoing"};
				let renderedEvent = TestUtils.renderIntoDocument(<Event event={event} />);
				const organizer = renderedEvent.refs.organizer;
				assert.equal(organizer.textContent, event.organizer);
			});
			
			it('that has the correct event date', () => {
				var event = {title:"This is an event", organizer:"NUS", dateTime:"Ongoing"};
				let renderedEvent = TestUtils.renderIntoDocument(<Event event={event} />);
				const dateTime = renderedEvent.refs.datetime;
				assert.equal(dateTime.textContent, event.dateTime);
			});
			
		});
		
	});
	
	describe('EventList Component', () => {
		
		describe('can render a table', () => {
			
			before(function() {
				const FAKE_DOM_HTML = '<html><body></body></html>';
				if (typeof document !== 'undefined') {
					return;
				}
				global.document = jsdom.jsdom(FAKE_DOM_HTML);
				global.window = document.defaultView;
				global.navigator = window.navigator;
			});
			
			beforeEach(function() {
				
			});
			
			it('that has no rows', () => {
				var events = [];
				let renderedEventList = TestUtils.renderIntoDocument(<EventList events={events} />);
				const rows = TestUtils.scryRenderedComponentsWithType(renderedEventList, Event);
				assert.equal(rows.length, events.length);
			});
			
			it('that has rows', () => {
				var events = [ {"title":"This is an event", "organizer":"NUS", "dateTime":"Ongoing"}
								];
				let renderedEventList = TestUtils.renderIntoDocument(<EventList events={events} />);
				const rows = TestUtils.scryRenderedComponentsWithType(renderedEventList, Event);
				assert.equal(rows.length, events.length);
			});
			
		});
		
	});
	
	describe('EventForm Component', () => {

		before(function() {
			const FAKE_DOM_HTML = '<html><body></body></html>';
			if (typeof document !== 'undefined') {
				return;
			}
			global.document = jsdom.jsdom(FAKE_DOM_HTML);
			global.window = document.defaultView;
			global.navigator = window.navigator;
		});
		
		beforeEach(function() {
			
		});
		
		it('can perform callback on search button press', function(done) {
			var onSubmit = function() { done(); };
			let renderedEventForm = TestUtils.renderIntoDocument(<EventForm handleSearchClick={onSubmit} />);
			const searchButton = renderedEventForm.refs.searchButton;
			TestUtils.Simulate.click(searchButton);
		});
		
		it('can perform callback on undo search button press', function(done) {
			var onSubmit = function() { done(); };
			let renderedEventForm = TestUtils.renderIntoDocument(<EventForm undoSearch={onSubmit} />);
			const undoButton = renderedEventForm.refs.undoButton;
			TestUtils.Simulate.click(undoButton);
		});
		
		it('can correctly display search string on search bar' , () => {
			let string = "Paid Sur";
			let renderedEventForm = TestUtils.renderIntoDocument(<EventForm searchString={string} />);
			const searchBar = renderedEventForm.refs.searchBar;
			assert.equal(searchBar.value, string);
		});
		
	});
	
	describe('The application', () => {
		
		before(function() {
			const FAKE_DOM_HTML = '<html><body></body></html>';
			if (typeof document !== 'undefined') {
				return;
			}
			global.document = jsdom.jsdom(FAKE_DOM_HTML);
			global.window = document.defaultView;
			global.navigator = window.navigator;
		});
		
		beforeEach(function() {
			
		});
		
		/* commented code as EventViewWrapper has failing imports which cannot be solved (import works on the actual app though) */
		/* 
		 * General pseudo code:
		 * 1. render application into a fake DOM
		 * 2. set search bar into desired search test
		 * 3. simulate clicking of search button
		 * 4. get rendered Event components
		 * 5. check that the count is equal to 1
		 */
		it('can search for events', () => {
			let events = [ {"title":"This is an event", "organizer":"NUS", "dateTime":"Ongoing"},
							{"title":"This is an event", "organizer":"NTU", "dateTime":"Ongoing"}
							];
			let searchText = "NUS";
			// let renderedApplication = TestUtils.renderIntoDocument(<EventViewWrapper events={events} />);
			// renderedApplication.refs.searchBar.value = searchText;
			// const searchButton = renderedEventForm.refs.searchButton;
			// TestUtils.Simulate.click(undoButton);
			// const events = TestUtils.scryRenderedComponentsWithType(renderedEventList, Event);
			// assert.equal(events.length, 1);
			
		});
		
	});
	
}