import { Meteor } from 'meteor/meteor';
import { Accounts } from 'meteor/accounts-base';

module.exports = {
    isLoggedIn() {
        return Meteor.userId();
    },
    logIn(userName, password, callback) {
        Meteor.loginWithPassword(userName, password, function(err) {
            callback(err);
        });
    },
    createUserPW(username, name, password, callback) {
        Accounts.createUser({
            username: username,
            password: password,
            profile: {
                name: name
            }
        }, function(err) {
            callback(err);
        });
    },
    logOut(callback) {
        Meteor.logout(function(err) {
            callback();
        });
    }
};
