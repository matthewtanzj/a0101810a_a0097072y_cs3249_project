import { Meteor } from 'meteor/meteor';
import { Router, Route, browserHistory } from 'react-router';
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Auth from '../../api/client/auth';

export default class LoginField extends Component {
    constructor (props) {
        super(props);

        this.state = {
            password: "",
            username: ""
        };
    }

    handleChange(key) {
        return function(e) {
            var state ={};
            state[key] = e.target.value;
            this.setState(state);
        }.bind(this);
    }

    handleSubmit(event) {
        event.preventDefault();
        Auth.logIn(this.state.username, this.state.password, function(err) {
            if(err) {
                if (err.error=='403') {
                    console.log("User does not exist do you want to create a new account?");
                    console.log(err);
                } else {
                    console.log("Unknown error.." + err);
                    console.log(err);
                }
            } else {
                console.log("Successfully logged in");
                browserHistory.push('/');
            }
        });
    }

    render() {
        return (
            <form className="form-horizontal" onSubmit={this.handleSubmit.bind(this)} >
                <div className="form-group">
                    <label className="col-sm-2 control-label">Username</label>
                    <div className="col-sm-10">
                        <input type="text" className="form-control" ref="inputUserName" onChange={this.handleChange('username')} placeholder="Username" />
                    </div>
                </div>
                <div className="form-group">
                    <label for="inputPassword" className="col-sm-2 control-label">Password</label>
                    <div className="col-sm-10">
                        <input type="password" className="form-control" id="inputPassword" ref="inputPassword" onChange={this.handleChange('password')} placeholder="Password"/>
                    </div>
                </div>
                <div className="form-group">
                    <div className="col-sm-offset-3 col-sm-6">
                        <button type="submit" className="btn btn-primary btn-block" >Log In</button>
                    </div>
                </div>
            </form>
        );
    }
}