import React, { Component } from 'react';

export default class NotFoundWrapper extends Component {
    render() {
        return (
            <div>
                <div className="row">
                    <div className="col-md-offset-4 col-md-4">
                        <h1>
                            Oops!</h1>
                        <h2>
                            404 Not Found</h2>
                        <div class="error-details">
                            Sorry, an error has occured, Requested page not found!
                        </div>
                        <div class="error-actions">
                            <a href="/" class="btn btn-primary btn-lg">
                                <span class="glyphicon glyphicon-home" /> Take Me Home
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}