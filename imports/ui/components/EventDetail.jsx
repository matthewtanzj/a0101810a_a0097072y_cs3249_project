import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import { Meteor } from 'meteor/meteor';


export default class EventDetail extends Component {
    componentDidMount() {
        // console.log(this.props.singleEvent)
        // console.log("for each")
        this.props.singleEvent.forEach(function(currentValue, index, arr) {
            console.log(currentValue + " " + index + " " + arr );
        });
    }
    
    render() {
        return (
            <tbody>
                <tr>
                    <td>
                        <p>Title:</p>
                        <p>{this.props.singleEvent.title}</p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <p>Organizer:</p>
                        <p>{this.props.singleEvent.organizer}</p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <p>Tags:</p>
                        <p>{this.props.singleEvent.tags}</p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <p>Description:</p>
                        <p>{this.props.singleEvent.description}</p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <p>Date & Time:</p>
                        <p>{this.props.singleEvent.dateTime}</p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <p>Venue:</p>
                        <p>{this.props.singleEvent.venue}</p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <p>Price:</p>
                        <p>{this.props.singleEvent.price}</p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <p>Agenda:</p>
                        <p>{this.props.singleEvent.agenda}</p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <p>Contact:</p>
                        <p>{this.props.singleEvent.contact}</p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <p>Hits:</p>
                        <p>{this.props.singleEvent.hits}</p>
                    </td>
                </tr>
            </tbody>
        )
    }
}