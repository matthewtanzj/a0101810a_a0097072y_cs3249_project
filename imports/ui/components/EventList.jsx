import React, { Component, PropTypes } from 'react';
import { Meteor } from 'meteor/meteor';

import Event from './Event';

// This JSX file contains the React View Component for rendering the event collection into a table/list
export default class EventList extends Component {

    renderEvents() {
        return this.props.events.map((event) => (
            <Event key={event._id} event={event} />
        ));
    }
    
    render() {
        return (
            <div className="wrapper-event-list">
                <div className="col-md-12">
                    <table className="table table-hover table-striped" cellspacing="0" border="0">
                        <tbody>
                            <tr>
                                <th class="text-center" scope="col" style={{width:"40px"}}>View</th>
                                <th scope="col" style={{width:"40%"}}><a id="clickable" onClick={() => this.props.sortTable("title")}>Event Title</a></th>
                                <th scope="col" style={{width:"40%"}}><a id="clickable" onClick={() => this.props.sortTable("organizer")}>Event Organizer</a></th>
                                <th scope="col">Event Date</th>
                            </tr>
                            {this.renderEvents()}
                        </tbody>
                    </table>
                </div>
            </div>
        );
    }
}

EventList.propTypes = {
    events: PropTypes.array.isRequired,
};