import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import { Meteor } from 'meteor/meteor';
import { Router, Route, browserHistory } from 'react-router';
import { createContainer } from 'meteor/react-meteor-data';
import EventDetail from './EventDetail.jsx';
import { Events } from '../../api/lib/events';
import RowDisplay from './view-event/RowDisplay';

// This React component SingleEventDetailWrapper contains all other event detail components for a single event
export default class EventSingleDetailWrapper extends Component {

    closeWindow() {
        window.close();
    }

    render() {
        return (
            <div>
            {this.props.singleEvent.length !== 0 ? 
                <div className="event-single-detail-wrapper col-md-12">
                    <div className="panel panel-default">
                        {/* Header */}
                        <div className="panel-heading">
                            <button type="button" className="close" aria-hidden="true" onClick={this.closeWindow}>×</button>
                            <h4><span className="popupBannerHeaderBottom-txt">Student Events</span></h4>
                        </div>
                        {/* Table content */}
                        <div className="panel-body">
                            <table className="table table-hover" border="0">
                                <tbody>
                                <RowDisplay label="Title:" value={this.props.singleEvent[0].title} />
                                <RowDisplay label="Organizer:" value={this.props.singleEvent[0].organizer} />
                                <RowDisplay label="Tags:" value={this.props.singleEvent[0].tags} />
                                <RowDisplay label="Description:" value={this.props.singleEvent[0].description} />
                                <RowDisplay label="Date & Time:" value={this.props.singleEvent[0].dateTime} />
                                <RowDisplay label="Venue:" value={this.props.singleEvent[0].venue} />
                                <RowDisplay label="Price:" value={this.props.singleEvent[0].price} />
                                <RowDisplay label="Agenda:" value={this.props.singleEvent[0].agenda} />
                                <RowDisplay label="Contact:" value={this.props.singleEvent[0].contact} />
                                </tbody>
                            </table>
                        </div>
                        {/* Close button */}
                    </div>
                    <div className="pull-right">
                        <button type="button" className="btn btn-primary" onClick={this.closeWindow}>
                            <span className="glyphicon glyphicon-remove" />
                            CLOSE
                        </button>
                    </div>
                </div>
            :""}
            </div>
        )
        
    }
}

EventSingleDetailWrapper.propTypes = {
    singleEvent: PropTypes.array.isRequired
};

export default createContainer(({ params }) => {
    Meteor.subscribe('event', params.id);
    return {
        singleEvent: Events.find({_id: params.id}).fetch(),
    };
}, EventSingleDetailWrapper);