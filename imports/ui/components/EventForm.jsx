import React, { Component, PropTypes } from 'react';
import { Router, Route, browserHistory } from 'react-router';
import { Meteor } from 'meteor/meteor';

/*
 * This JSX file contains the EventForm React View Component:
 * A react component that represents the top portion of the app that handles
 *      1. Creation of new Events
 *      2. Searching of events
 *      3. Displaying number of events
 */ 
export default class EventForm extends Component {
    
    createEvent(e) {
        browserHistory.push('/create_event');
    }

    render() {
        return (
            <div className="eventForm col-md-12">
                <span className="col-md-3">
                    <button type="button" className="btn btn-success" data-toggle="popover" data-content="Create a new event request." onClick={this.createEvent.bind(this)} >
                        <span className="glyphicon glyphicon-plus"/>&nbsp;Create Event
                    </button>
                </span>
                <span className="col-md-6" id="search-group">
                    <input className="form-control" ref="searchBar" style={{borderTopRightRadius:"0px", borderBottomRightRadius: "0px"}} id="search-form" type="text" value={this.props.searchString} onChange={this.props.updateSearchString} placeholder="Type here" />
                    <a className="btn btn-default" ref="searchButton" style={{borderRadius:"0px"}} onClick={this.props.handleSearchClick}>
                        <span className="glyphicon glyphicon-search" style={{color: 333333}}/>
                    </a>
                    <a className="btn btn-default" ref="undoButton" style={{borderTopLeftRadius:"0px", borderBottomLeftRadius: "0px"}} title="Undo all search and sort." onClick={this.props.undoSearch}>
                        <span className="glyphicon glyphicon-repeat" style={{color: 333333}}/>
                    </a>
                </span>
                <span className="col-md-3 text-right form-inline">
                    <div className="navbar-btn">Total {this.props.eventCount} items&nbsp;&nbsp;&nbsp;&nbsp;</div>
                </span>
            </div>
        );
    }
}

EventForm.propTypes = {
    updateSearchString: PropTypes.func.isRequired,
    eventCount: PropTypes.number.isRequired,
    searchString: PropTypes.string.isRequired,
    handleSearchClick: PropTypes.func.isRequired,
    undoSearch: PropTypes.func.isRequired,
};