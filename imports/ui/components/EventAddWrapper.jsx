import React, { Component, PropTypes } from 'react';
import { Meteor } from 'meteor/meteor';
import { Router, Route, browserHistory } from 'react-router';

import InputBox from './add-event/InputBox.jsx';
import TextAreaBox from './add-event/TextAreaBox.jsx';
import DropDownBox from './add-event/DropDownBox.jsx';
import DateTimeBox from './add-event/DateTimeBox.jsx';
import Navbar from './Navbar.jsx';

// This JSX file contains the React View Component for a new event creation
export default class EventAddWrapper extends Component {
    
    // set initial states
    constructor(props) {
        super(props);
    
        this.state = {
            title: '',
            organizer: '',
            committee: 'NUSSU',
            category: 'Bashes',
            tags: '',
            displayStart: new Date(Date.now()),
            displayEnd: new Date(Date.now()),
            description: '',
            dateTime: '',
            venue: '',
            price: '',
            agenda: '',
            contact: ''
        };
    }


    handleChange(key) {
        return function (e) {
            var state = {};
            state[key] = e.target.value;
            this.setState(state);
        }.bind(this);
    }
    
    handleStartDateTimeChange(newDate) {
        var timestamp = parseInt(newDate);
        var d = new Date(timestamp);
        var dateString = d.toString();
        var state = {};
        state['displayStart'] = dateString;
        this.setState(state);
    }
    
    handleEndDateTimeChange(newDate) {
        var timestamp = parseInt(newDate);
        var d = new Date(timestamp);
        var dateString = d.toString();
        var state = {};
        state['displayEnd'] = dateString;
        this.setState(state);
    }
    
    addEvent(event) {
        if (this.state.title.length === 0 || this.state.organizer.length === 0) {
            alert("Please ensure that both Event Title and Organizer fields are filled in.");
        } else if (this.formIsValid()) {
            console.log(this.state);
            Meteor.apply('events.insert', [this.state], function() {
                console.log("Successfully added an event!");
                browserHistory.push('/');
                });
        } else {
            console.log("Invalid fields in form");
        }
    }
    
    formIsValid() {
        return !!(this.state.title.length > 0 &&
        this.state.title.length < 200 &&
        this.state.organizer.length > 0 &&
        this.state.organizer.length < 100 &&
        this.state.dateTime.length <= 500 &&
        this.state.venue.length <= 500 &&
        this.state.price.length <= 500 &&
        this.state.agenda.length <= 500 &&
        this.state.contact.length <= 500);
    }
    
    returnToHome(e) {
        browserHistory.push('/');
    }
    
    render() {
        
        let committees = ["NUSSU", "Faculty Clubs", "Halls of Residences", "Clubs & Societies", "NUS", "Interest Groups", "Others"];
        let categories = ["Bashes", "Bazaars", "Competitions/Tournamemts", "Sports & Recreations", "Performances", "Announcements", "Excursions", "Exhibitions", "Courses/Workshops", "Recruitment", "Administration", "Charity", "Others"];
        
        return (
            <div className="addWrapper">
                <Navbar />
                <div className="row">
                    <div className="panel-group col-md-12">
                        {/* Top Panel: General */}
                        <div className="panel panel-default">
                            {/* Top Panel: Panel Heading */}
                            <div className="panel-heading">
                                <h4 className="panel-title">
                                    <a className="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true">
                                        General
                                    </a>
                                </h4>
                            </div>
                            {/* Top Panel: Panel Content */}
                            <div id="collapseOne" className="panel-collapse collapse in" aria-expanded="true">
                                <div className="panel-body">
                                    <table className="table table-hover">
                                        <tbody>
                                            <InputBox label={"Event Title"} isRequired={true} content={this.state.title} handleChange={this.handleChange('title')} maxChar={200}/>
                                            {/* Top Panel: Organizer */}
                                            <InputBox label={"Organizer"} isRequired={true} content={this.state.organizer} handleChange={this.handleChange('organizer')} maxChar={100} />
                                            {/* Top Panel: Committee */}
                                            <DropDownBox label={"Committee"} selected={this.state.committee} options={committees} handleChange={this.handleChange('committee')} />
                                            {/* Top Panel: Category */}
                                            <DropDownBox label={"Category"} selected={this.state.category} options={categories} handleChange={this.handleChange('category')} />
                                            {/* Top Panel: Tags */}
                                            <InputBox label={"Tags"} isRequired={false} content={this.state.tags} handleChange={this.handleChange('tags')} />
                                            {/* Top Panel: Display Start */}
                                            <DateTimeBox label={"Display Start"} handleChange={this.handleStartDateTimeChange.bind(this)} />
                                            {/* Top Panel: Display End */}
                                            <DateTimeBox label={"Display End"} handleChange={this.handleEndDateTimeChange.bind(this)} />
                                            {/* Top Panel: Description */}
                                            <TextAreaBox label={"Description"} content={this.state.description} handleChange={this.handleChange('description')} />
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        {/* Bottom Panel: Event Details */}
                        <div className="panel panel-default">
                            {/* Bottom Panel: Panel Heading */}
                            <div className="panel-heading">
                                <h4 className="panel-title">
                                    <a className="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="true">
                                        Event Details
                                    </a>
                                </h4>
                            </div>
                            {/* Bottom Panel: Panel Content */}
                            <div id="collapseTwo" className="panel-collapse collapse in" aria-expanded="true">
                                <div className="panel-body">
                                    <table className="table table-hover">
                                        <tbody>
                                            {/* Bottom Panel: Date & Time */ }
                                            <TextAreaBox label={"Date & Time"} placeholder={"Specify the actual event schedule here"} content={this.state.dateTime} handleChange={this.handleChange('dateTime')} maxChar={500} />
                                            {/* Bottom Panel: Venue */}
                                            <TextAreaBox label={"Venue"} content={this.state.venue} handleChange={this.handleChange('venue')} maxChar={500} />
                                            {/* Bottom Panel: Price */}
                                            <TextAreaBox label={"Price"} content={this.state.price} handleChange={this.handleChange('price')} maxChar={500} />
                                            {/* Bottom Panel: Agenda */}
                                            <TextAreaBox label={"Agenda"} content={this.state.agenda} handleChange={this.handleChange('agenda')} maxChar={500} />
                                            {/* Bottom Panel: Contact */}
                                            <TextAreaBox label={"Contact"} content={this.state.contact} handleChange={this.handleChange('contact')} maxChar={500} />
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        {/* Button Row */}
                        <div className="row">
                            <div className="text-right col-md-12">
                                <button onClick={this.returnToHome.bind(this)} id="add-event-btn" type="button" className="btn btn-default" data-loading-text="Cancel...">
                                    <span className="glyphicon glyphicon-remove-circle" />&nbsp;Cancel
                                </button>
                                <button onClick={this.addEvent.bind(this)} id="add-event-btn" type="button" className="btn btn-primary" data-loading-text="Saving...">
                                    <span className="glyphicon glyphicon-floppy-save" />&nbsp;Save
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}