import React, { Component, PropTypes } from 'react';
import { Meteor } from 'meteor/meteor';
import { Router, Route, browserHistory } from 'react-router';

import Auth from '../../api/client/auth';

export default class Navbar extends Component {
    handleLogout() {
        Auth.logOut(function(err) {
            if(!err) {
                console.log("You have successfully logged out");
                browserHistory.push('/login');
            } else {
                console.log("Error logging out...");
                console.log(err);
            }
        });
    }

    render() {
        return (
            <div className="row top-bar" style={{paddingBottom: "50px"}}>
                <div className="col-md-10 pull-left">
                    <h2>IVLE Events</h2>
                </div>
                <div className="col-md-2">
                    <div className="btn-group pull-right" style={{marginTop: "20px"}}>
                        <button type="button" className="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            { Meteor.user().profile.name } <span className="caret"/>
                        </button>
                        <ul className="dropdown-menu">
                            <li><a onClick={this.handleLogout}>Logout</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        )
    }
}
