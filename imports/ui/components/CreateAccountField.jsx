import { Meteor } from 'meteor/meteor';
import { Router, Route, browserHistory } from 'react-router';
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Auth from '../../api/client/auth';

export default class CreateAccountField extends Component {
    constructor(props) {
        super(props);

        this.state = {
            username: "",
            name: "",
            password: ""
        };
    }

    handleChange(key) {
        return function(e) {
            var state = {};
            state[key] = e.target.value;
            this.setState(state);
        }.bind(this);
    }

    handleSubmit(event) {
        event.preventDefault();
        const username = this.state.username;
        const password = this.state.password;
        const name = this.state.name;
        Auth.createUserPW(username, name, password, function(err) {
            if(err) {
                console.log("Error while creating user.." + err);
                console.log(err);
                // this.setState({feedback: err.reason});
            } else {
                Auth.logIn(username, password, function(err) {
                    if(!err) {
                        browserHistory.push('/');
                    }
                });
            }
        });
    }

    render() {
        return (
            <form className="form-horizontal" onSubmit={this.handleSubmit.bind(this)} >
                <div className="form-group">
                    <label className="col-sm-2 control-label">Username</label>
                    <div className="col-sm-10">
                        <input type="text" className="form-control" ref="inputUserName" onChange={this.handleChange("username")} placeholder="Username" />
                    </div>
                </div>
                <div className="form-group">
                    <label for="inputName" className="col-sm-2 control-label">Name</label>
                    <div className="col-sm-10">
                        <input type="text" className="form-control" id="inputName" ref="inputName" onChange={this.handleChange("name")} placeholder="John"/>
                    </div>
                </div>
                <div className="form-group">
                    <label for="inputPassword" className="col-sm-2 control-label">Password</label>
                    <div className="col-sm-10">
                        <input type="password" className="form-control" id="inputPassword" ref="inputPassword" onChange={this.handleChange("password")} placeholder="Password"/>
                    </div>
                </div>
                <div className="form-group">
                    <div className="col-sm-offset-3 col-sm-6">
                        <button type="submit" className="btn btn-warning btn-block" >Sign Up</button>
                    </div>
                </div>
            </form>
        );
    }
}