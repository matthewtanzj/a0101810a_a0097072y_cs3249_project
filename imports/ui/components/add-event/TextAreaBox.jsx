import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import { Meteor } from 'meteor/meteor';

// This JSX file contains the React View Component for displaying text area boxes
export default class TextAreaBox extends Component {
    
    render() {
        return (
            <tr>
                <td>
                    <label id="" className="col-md-3 text-right">
                        {this.props.label}
                    </label>
                    <div className="col-md-8">
                        <textarea name="date-time" rows="2" cols="20" id="" className="form-control" value={this.props.content} placeholder={this.props.placeholder} onChange={this.props.handleChange} style={{height:"50px"}}></textarea>
                        <span id="error-msg">{this.props.content.length > this.props.maxChar ? this.props.label + " allows maximum of " + this.props.maxChar + " characters." : "" }</span>
                    </div>
                    <div className="col-md-1">
                        <span id="" className="iName-txt">Char. Count:</span>
                        <input name="" value={this.props.content.length} type="text" readOnly="readonly" id="" className="form-control" />
                    </div>
                </td>
            </tr>
        );
    }
}

TextAreaBox.propTypes = {
    label: PropTypes.string.isRequired,
    content: PropTypes.string.isRequired,
    handleChange: PropTypes.func.isRequired,
};