import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import { Meteor } from 'meteor/meteor';

// This JSX file contains the React View Component for displaying date-time boxes
export default class DateTimeBox extends Component {
    
    render() {
        
        var DateTimeField = require('react-bootstrap-datetimepicker');
        
        return (
            <tr>
                <td>
                    <label id="" className="col-md-3 text-right">
                        {this.props.label}
                    </label>
                    <div className="col-md-3">
                        <DateTimeField
                            onChange = {this.props.handleChange}
                            inputFormat = {"DD-MM-YY h:mm A"}
                        />
                    </div>
                </td>
            </tr>
        );
    }
}

DateTimeBox.propTypes = {
    label: PropTypes.string.isRequired,
    handleChange: PropTypes.func.isRequired,
};
