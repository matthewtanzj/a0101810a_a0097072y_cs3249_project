import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import { Meteor } from 'meteor/meteor';

// This JSX file contains the React View Component for displaying simple input boxes
export default class InputBox extends Component {
    
    render() {
        return (
            <tr>
                <td>
                    <label id="" className="col-md-3 text-right">
                        {this.props.isRequired ? <span style={{color: "red"}}>* </span>: ""}
                        {this.props.label}
                    </label>
                    <div className="col-md-9">
                        <input name="event-title" value={this.props.content} placeholder={this.props.placeholder} onChange={this.props.handleChange} type="text" maxlength="200" id="" className="form-control"/>
                        <span id="error-msg">{this.props.content.length > this.props.maxChar ? this.props.label + " allows maximum of " + this.props.maxChar + " characters." : "" }</span>
                    </div>
                </td>
            </tr>
        );
    }
}

InputBox.propTypes = {
    label: PropTypes.string.isRequired,
    isRequired: PropTypes.bool.isRequired,
    content: PropTypes.string.isRequired,
    handleChange: PropTypes.func.isRequired,
};