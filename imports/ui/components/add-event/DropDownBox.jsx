import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import { Meteor } from 'meteor/meteor';

// This JSX file contains the React View Component for displaying drop-down boxes
export default class DropDownBox extends Component {
    
    render() {
        return (
            <tr>
                <td>
                    <label id="" className="col-md-3 text-right">
                        {this.props.label}
                    </label>
                    <div className="col-md-3">
                        <select name="" id="" className="form-control" value={this.props.selected} onChange={this.props.handleChange}>
                            {
                                this.props.options.map(function(option) {
                                    return <option key={option} value={option}>{option}</option>
                                })
                            }
                        </select> 
                    </div>
                </td>
            </tr>
        );
    }
}

DropDownBox.propTypes = {
    label: PropTypes.string.isRequired,
    options: PropTypes.array.isRequired,
    handleChange: PropTypes.func.isRequired,
};