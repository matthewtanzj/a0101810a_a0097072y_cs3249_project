import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import { Meteor } from 'meteor/meteor';

// This JSX file contains the React View Component for displaying individual row in Single Event View
export default class RowDisplay extends Component {
    
    render() {
        return (
            <tr>
                <td>
                    <label className="col-md-3 control-label">
                        <div className="pull-right">{this.props.label}</div>
                    </label>
                    <div className="col-md-9">
                        <span className="iItem-txt">
                            {this.props.value.length !== 0 ? this.props.value : "-"}
                        </span>
                    </div>
                </td>
            </tr>
        );
    }
}

RowDisplay.propTypes = {
    label: PropTypes.string.isRequired,
    value: PropTypes.string.isRequired,
};