import React, { Component } from 'react';
import LoginField from './LoginField';
import CreateAccountField from './CreateAccountField';

export default class Login extends Component {
    render() {
        return (
            <div style={{textAlign:"center", paddingTop:"160px"}}>
                <h1>Ivle Events</h1>
                <div className="row">
                    <div className="col-md-offset-4 col-md-4" style={{textAlign:"left"}}>
                        <LoginField />
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-offset-4 col-md-4" style={{textAlign:"left"}}>
                        <CreateAccountField />
                    </div>
                </div>
            </div>
        );
    }
}