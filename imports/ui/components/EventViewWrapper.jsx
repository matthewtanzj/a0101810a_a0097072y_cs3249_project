import React, { Component, PropTypes } from 'react';
import { Meteor } from 'meteor/meteor';
import { createContainer } from 'meteor/react-meteor-data';
import { Router, Route, browserHistory } from 'react-router';

import { Events } from '../../api/lib/events';
import EventList from './EventList';
import EventForm from './EventForm';
import Navbar from './Navbar.jsx';

// This React component EventViewWrapper contains all other event components
export default class EventView extends Component {
    // set initial states
    constructor(props) {
        super(props);

        this.state = {
            sortString: "",
            sortByAscending: false,
            searchString: "",
            clickedSearch: false
        };
    }

    sortBy(field, reverse, primer) {
        var key = function (x) {return primer ? primer(x[field]) : x[field]};

        return function (a,b) {
            var A = key(a), B = key(b);
            return ( (A < B) ? -1 : ((A > B) ? 1 : 0) ) * [-1,1][+!!reverse];
        }
    }

    setSortString(sortString) {
        // assume a new column getting sorted, sort by ascending order
        let newSortOrder = true;
        // if sorting by same column, reverse sort order
        if (sortString === this.state.sortString) {
            newSortOrder = !this.state.sortByAscending;
        }
        this.setState({sortString: sortString});
        this.setState({sortByAscending: newSortOrder});
    }
    
    updateSearchString() {
        return function (e) {
            this.setState({searchString: e.target.value});
        }.bind(this);
    }
    
    handleSearchClick() {
        return function (e) {
            this.setState({clickedSearch: true});
        }.bind(this);
    }
    
    resetSearch() {
        return function (e) {
            this.setState({clickedSearch: false});
            this.setState({searchString: ""});
        }.bind(this);
    }

    render() {
        /* Filter events based on searchString */
        let searchString = this.state.searchString.trim().toLowerCase().replace(/([!@#$%^&*()+=\[\]\\';,./{}|":<>?~_-])/g, "\\$1"); // process searchString for matching
        let events = this.props.events;
        /* remove "&& this.state.clickedSearch for live search" */
        {this.state.searchString.length > 0 && this.state.clickedSearch ? events = events.filter(function(l) {
                                                        return l.title.toLowerCase().match(searchString) || l.organizer.toLowerCase().match(searchString)
                                                    }) : ""
        }
        {/* Sorts the events based on 2 states: sortString & sortByAscending */}
        {this.state.sortString.length > 0 ? events.sort(this.sortBy(this.state.sortString, this.state.sortByAscending, function(a){return a.toUpperCase()})) : ""}
        
        return (
            <div className="wrapper-event-view">
                <div className="col-md-12">
                    <Navbar />
                    <div className="row event-options-row">
                        <EventForm eventCount={events.length} searchString={this.state.searchString} updateSearchString={this.updateSearchString()} handleSearchClick={this.handleSearchClick()} undoSearch={this.resetSearch()} />
                    </div>
                    <div className="row event-results">
                        <EventList events={events} sortTable={this.setSortString.bind(this)} />
                    </div>
                </div>
            </div>
        );
    }
}

EventView.propTypes = {
    events: PropTypes.array.isRequired
};

export default createContainer(() => {
    Meteor.subscribe('events');
    return {
        events: Events.find({}).fetch(),
        numEvents: Events.find({}).count()
        //currentUser: Meteor.user(),
    };
}, EventView);