import React, { Component, PropTypes } from 'react';
import { Meteor } from 'meteor/meteor';

// This JSX file contains the React View Component for rendering individual event into a table/list
export default class Event extends Component {
    openEvent() {
        var w = 1000;
        var h = 600;
        var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
        var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

        var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
        var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

        var left = ((width / 2) - (w / 2)) + dualScreenLeft;
        var top = ((height / 2) - (h / 2)) + dualScreenTop;
        window.open("/event/"+this.props.event._id, "_blank", "width="+w+", height="+h+", left="+left+", top="+top);
    }

    render() {
        return (
            <tr>
                <td className="text-center">
                    <button onClick={this.openEvent.bind(this)} style={{"background":"transparent", "border":"none"}}>
                        <span className="glyphicon glyphicon-eye-open black" style={{"color":"black"}} />
                    </button>
                </td>
                <td ref="title">{this.props.event.title}</td>
                <td ref="organizer">{this.props.event.organizer}</td>
                <td ref="datetime">{this.props.event.dateTime}</td>
            </tr>
        );
    }
}

Event.propTypes = {
    // This component gets the task to display through a React prop.
    // We can use propTypes to indicate it is required
    event: PropTypes.object.isRequired
};