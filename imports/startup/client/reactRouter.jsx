/**
 * Created by John on 4/18/2016.
 */
import React from 'react';
import { Router, Route, browserHistory } from 'react-router';

// route components
import EventSingleDetailWrapper from '../../ui/components/EventSingleDetailWrapper.jsx';
import EventViewWrapper from '../../ui/components/EventViewWrapper.jsx';
import LoginWrapper from '../../ui/components/LoginCreateWrapper.jsx';
import EventAddWrapper from '../../ui/components/EventAddWrapper.jsx';
import NotFoundWrapper from '../../ui/components/NotFoundWrapper.jsx';
import Auth from '../../api/client/auth.js';

const isLoggedIn = function(nextState, replace) {
    if(!Auth.isLoggedIn()) {
        replace({
            pathname: 'login',
            state: {nextPathname: nextState.location.pathname}
        });
    }
};

export const renderRoutes = () => (
    <Router history={browserHistory}>
        <Route path="/" component={EventViewWrapper} onEnter={isLoggedIn} />
            <Route path="event/:id" component={EventSingleDetailWrapper} onEnter={isLoggedIn} />
        <Route path="login" component={LoginWrapper} />
        <Route path="create_event" component={EventAddWrapper} onEnter={isLoggedIn} />
        <Route path="*" component={NotFoundWrapper} />
    </Router>
);